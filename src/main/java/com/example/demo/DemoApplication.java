package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.util.UUID;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}

@Component
@AllArgsConstructor
@Slf4j
class Starter implements CommandLineRunner {
	private final UserRepository repository;

	@Override
	public void run(String... args) throws Exception {
		/**
		 * docker run --rm --name moje-mongo -p 27017:27017 mongo
		 */

		String maxId = UUID.randomUUID().toString();
		User max = new User(maxId, "Max", "max@gmail.com");
		repository.save(max);
		String mimiId = UUID.randomUUID().toString();
		User mimi = new User(mimiId, "Mimi", "mimi@gmail.com");
		repository.save(mimi);


		log.info("Szukam Maxa po jego id");
		User result1 = repository.findOne(maxId);
		log.info("Imię znalezionego usera: {}", result1.getName());

		log.info("Szukam Mimi po emailu");
		User result2 = repository.findByEmail("mimi@gmail.com");
		log.info("Imię znalezionego usera: {}", result2.getName());
	}
}